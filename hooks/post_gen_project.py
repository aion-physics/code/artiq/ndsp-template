import logging
import platform
import shlex
import subprocess as sp
import sys
from pathlib import Path

logging.basicConfig(level=logging.DEBUG)

make_venv = False
update_nixpkgs = False


def main():
    on_windows = platform.system() == "Windows"
    if on_windows:
        venv_prefix = Path("venv/Scripts/")
    else:
        venv_prefix = Path("venv/bin/")

    commands = (
        [
            # Git setup
            "git init",
            "git add .",
            'git commit -m "Cookiecutter systematic init"',
            # Install poetry
            "python -m venv venv",
            "{} -m pip install -U pip setuptools wheel poetry".format(
                venv_prefix / "python"
            ),
            # Relock poetry and install requirements
            "{} -m poetry lock".format(venv_prefix / "python"),
            "{} -m poetry install".format(venv_prefix / "python"),
            "git add .",
            "bash -c \"git commit -am 'Update poetry lock' || true\"",
            # Autoupdate pre-commit and lint
            "{} -m poetry run pre-commit autoupdate".format(venv_prefix / "python"),
            'bash -c "{} -m poetry run pre_commit run --all || true"'.format(
                venv_prefix / "python"
            ),
            "bash -c \"git commit -am 'Lint' || true\"",
        ]
        + (
            [
                # Remove the venv if it wasn't requested
                "git clean -xdf",
            ]
            if not make_venv
            else []
        )
        + [
            'git tag -am "Initial setup" v0.0',
        ]
    )

    commands = [shlex.split(c, posix=(not on_windows)) for c in commands]

    logging.debug(platform.system())
    logging.debug(commands)

    for cmd in commands:
        logging.debug(cmd)
        process = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.STDOUT)
        process.wait()

        output, _ = process.communicate()
        lines = output.decode("utf-8").splitlines()

        logging.debug("\n".join(lines))

        if process.returncode != 0:
            err = "Command '{}' failed: aborting install".format(cmd)
            logging.error(err)
            raise RuntimeError(err)


if __name__ == "__main__":
    main()
