import contextlib
import os
import platform
import shlex
import logging
import subprocess as sp
from pathlib import Path

import pytest


logging.basicConfig(level=logging.DEBUG)


@pytest.fixture(scope="module")
def on_windows():
    return platform.system() == "Windows"


@pytest.fixture(scope="module")
def venv(on_windows):
    if on_windows:
        venv_prefix = Path("venv/Scripts/")
    else:
        venv_prefix = Path("venv/bin/")

    return venv_prefix


@pytest.fixture(scope="module")
def new_project(tmpdir_factory, venv):
    tmp_path = Path(tmpdir_factory.mktemp("proj"))

    path_to_package = (Path(__file__) / "../../").resolve()

    with cd(tmp_path):

        # Make a new project with the default settings
        commands = [
            [
                "cookiecutter",
                path_to_package,
                "--no-input",
                "-v",
                "package_name=testproj",
            ],
        ]

        run_and_check(commands, on_windows)

        path_to_proj = tmp_path / "testproj"

        with cd(path_to_proj):
            # Generate a venv with all the dependencies installed
            run_and_check(
                [
                    "python -m venv venv",
                    str(venv / "python") + " -m pip install poetry",
                    str(venv / "python") + " -m poetry install",
                ],
                on_windows,
            )

            yield path_to_proj


def run_and_check(cmds, on_windows):
    for c in cmds:
        if isinstance(c, list):
            split_cmd = c
        else:
            split_cmd = shlex.split(str(c), posix=(not on_windows))

        print("Command: {}".format(split_cmd))

        out = sp.run(split_cmd, stdout=sp.PIPE, stderr=sp.STDOUT)

        print("Output: {}".format(out.stdout.decode()))
        assert out.returncode == 0


def test_setup(new_project):
    pass


def test_venv(new_project, on_windows, venv):
    print(os.listdir(new_project))

    assert (new_project / venv).exists()

    print(os.listdir(new_project / venv))

    if on_windows:
        assert (new_project / venv / "python.exe").exists()
    else:
        assert (new_project / venv / "python").exists()


def test_tests(new_project, venv):
    run_and_check(
        [str(new_project / venv / "python") + " -m poetry run pytest"], on_windows
    )


def test_slow_tests(new_project, venv):
    run_and_check(
        [str(new_project / venv / "python") + " -m poetry run pytest --runslow"],
        on_windows,
    )


def test_git(new_project):
    run_and_check(["git rev-parse HEAD"], on_windows)


def test_docs_html(new_project, on_windows, venv):
    run_and_check(
        [
            str(new_project / venv / "python")
            + " -m poetry run sphinx-build docs public -b html"
        ],
        on_windows,
    )


def test_docs_latex(new_project, on_windows, venv):
    run_and_check(
        [
            str(new_project / venv / "python")
            + " -m poetry run sphinx-build docs public -b latex"
        ],
        on_windows,
    )


@contextlib.contextmanager
def cd(path):
    # https://stackoverflow.com/questions/24469538/sh-cd-using-context-manager
    old_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_path)
