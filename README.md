# pypackage

_Charles Baynham 2024_

A template for creating an ARTIQ NDSP. Uses poetry for package management.

## Usage

### With Nix

Change to the directory where you want to generate a project and then run
`nix run git+https://gitlab.com/aion-physics/code/artiq/ndsp-template.git`.

### Without Nix

You should have previously used git to clone something from gitlab.com server, so that
authentication is set up. Once this is done, use

    pip install cookiecutter
    cookiecutter https://gitlab.com/aion-physics/code/artiq/ndsp-template.git

(if you've previously set up SSH, you can also use `cookiecutter git@gitlab.com:aion-physics/code/artiq/ndsp-template.git`)

Fill out the README, and - if necessary - [choose a license](https://choosealicense.com/) for the project.

## Credits

This template was written by Charles Baynham <charles.baynham@gmail.com>
