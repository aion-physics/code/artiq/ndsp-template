Welcome to {{cookiecutter.package_name}}'s documentation!
=========================================================================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme
   usage

   autogen/modules
