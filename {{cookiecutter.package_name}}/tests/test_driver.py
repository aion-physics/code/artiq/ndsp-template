"""
Test the device as an NDSP server

These tests will fail unless you've registered a device simulator for your
driver - feel free to delete this file if you don't intend to do that.
"""
import logging
from time import sleep

import pytest

from {{cookiecutter.package_name}}.sipyco_controller import main as aqctl_main

logger = logging.getLogger(__name__)

test_dev = "COMNOTREAL"


@pytest.fixture
def client(raw_client, caplog):
    """
    Get a client to the running server and reset the device before and after usage

    Runs for every usage
    """

    # Set log level to DEBUG
    caplog.set_level(logging.DEBUG)

    raw_client.reset()
    yield raw_client
    raw_client.reset()


@pytest.fixture(scope="module")
def raw_client():
    """
    Start a server and return an RPC client pointing at the server

    Runs once per module
    """

    import asyncio
    import threading

    # Start the RPC server in another thread
    def wrapper():
        try:
            asyncio.get_event_loop()
        except RuntimeError:
            asyncio.set_event_loop(asyncio.new_event_loop())
        aqctl_main(
            extra_args=[
                "--id",
                test_dev,  # This is ignored, due to simulation mode
                "--simulation",  # Don't actually connect to any external devices
                # Explicitly bind to localhost to avoid IPv6 default which is not
                # supported on Gitlab CI
                "--bind",
                "127.0.0.1",
                # Override the default behaviour which is to add the IPv6 localhost
                # in addition to the main bind address
                "--no-localhost-bind",
            ]
        )

    t = threading.Thread(target=wrapper, name="server_thread")
    t.start()

    logger.info("Started {{ cookiecutter.driver_name }} server...")

    sleep(3)

    client = get_new_client()
    yield client

    # Fixure complete. Kill the server
    client.terminate()


def get_new_client():
    from sipyco.pc_rpc import Client

    return Client(host="127.0.0.1", port="3301")


# Uncomment this test to test a basic client connection as part of your unit tests
# def test_init(raw_client):
#     client.reset()
