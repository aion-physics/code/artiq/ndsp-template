from generic_scpi_driver import get_controller_func

from .driver import {{cookiecutter.driver_name}}

main = get_controller_func("{{cookiecutter.driver_name}}", 3301, {{cookiecutter.driver_name}})


if __name__ == "__main__":
    main()
