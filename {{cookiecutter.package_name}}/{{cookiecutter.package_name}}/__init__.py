"""{{cookiecutter.package_name}} - {{cookiecutter.package_description}}"""
from .driver import {{cookiecutter.driver_name}}

__author__ = "{{cookiecutter.author_name}} <{{cookiecutter.author_email}}>"
__all__ = ["{{cookiecutter.driver_name}}"]
__version__ = "0.1"
