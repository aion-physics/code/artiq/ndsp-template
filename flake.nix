{
  description = "Flake to convert this repo to a nix app";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        reqs = with pkgs;
          [ cookiecutter git pre-commit (python3.withPackages (ps: [ ps.pytest ])) ];
      in
      {
        devShell = pkgs.mkShell
          {
            buildInputs = reqs;
          };

        apps.default =
          let
            script = pkgs.writeShellScriptBin "generate" ''
              export PATH=${pkgs.lib.makeBinPath reqs}:$PATH
              export TMP_DIR=$(mktemp -d)

              # Copy this flake to the tmp dir and make it writable by the current user
              cp -a ${self}/. $TMP_DIR
              chmod -R u+w $TMP_DIR
              chmod -R g+w $TMP_DIR

              echo Generating a new ARTIQ NDSP in the directory $(pwd)...

              cookiecutter $TMP_DIR "$@"
            '';
          in
          {
            type = "app";
            program = "${script}/bin/generate";
          };

      }
    );
}
